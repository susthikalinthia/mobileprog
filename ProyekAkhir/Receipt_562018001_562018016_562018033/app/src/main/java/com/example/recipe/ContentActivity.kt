package com.example.recipe

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_content.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.login.*
import kotlinx.android.synthetic.main.user_registration.*
import kotlinx.android.synthetic.main.user_registration.save

class ContentActivity : AppCompatActivity() {

    lateinit var handler: DatabaseHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content)

        handler= DatabaseHelper(this)

        showHome()

        registration.setOnClickListener {
            showRegistration()
        }

        back.setOnClickListener {
            showHome()
        }

        login.setOnClickListener {
            showLogIn()
        }

        create_account.setOnClickListener {
            showRegistration()
        }

        save.setOnClickListener{
            handler.insertUser(name.text.toString(), email.text.toString(), password_register.text.toString())
            showHome()
        }



        login_button.setOnClickListener {

            if (handler.userPresent(login_email.text.toString(), login_password.text.toString())) {
                Toast.makeText(this, "Login Success", Toast.LENGTH_SHORT).show()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            }

            else
                Toast.makeText(this, "Username or password is incorrect", Toast.LENGTH_SHORT).show()
            }

    }

    private fun showRegistration() {
        registration_layout.visibility = View.VISIBLE
        login_layout.visibility=View.GONE
        home_11.visibility=View.GONE
    }

    private fun showLogIn() {
        registration_layout.visibility=View.GONE
        login_layout.visibility=View.VISIBLE
        home_11.visibility=View.GONE

    }

    private fun showHome() {
        registration_layout.visibility=View.GONE
        login_layout.visibility=View.GONE
        home_11.visibility=View.VISIBLE
    }



    }
