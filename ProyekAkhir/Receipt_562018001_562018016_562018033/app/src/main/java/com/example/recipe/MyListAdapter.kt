package com.example.recipe

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.example.recipe.R

class MyListAdapter(private val context: Activity, private val id: Array<String>, private val name: Array<String>, private val ingredientsAndDirections: Array<String>)
    : ArrayAdapter<String>(context, R.layout.custom_list, name) {

    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val rowView = inflater.inflate(R.layout.custom_list, null, true)

        val idText = rowView.findViewById(R.id.textViewId) as TextView
        val nameText = rowView.findViewById(R.id.textViewName) as TextView
        val IngredientsAndDirectionsText = rowView.findViewById(R.id.textViewIngredientsDirection) as TextView

        idText.text = "Id: ${id[position]}"
        nameText.text = "Name: ${name[position]}"
        IngredientsAndDirectionsText.text = "Ingredients And Directions: ${ingredientsAndDirections[position]}"
        return rowView
    }
}